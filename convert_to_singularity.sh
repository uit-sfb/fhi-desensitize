docker run \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v $(pwd)/singularity:/output \
  --privileged -t --rm \
  filo/docker2singularity \
  desensitize-standalone:latest
