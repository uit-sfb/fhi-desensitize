process Multiqc {

  publishDir "output/multiqc", mode: 'copy'
  conda '/opt/conda/envs/env'

  input:
  path before
  path screened
  path after

  output:
  path 'multiqc'

  shell:
  '''
  multiqc --outdir multiqc !{before} !{screened} !{after}
  '''
}
