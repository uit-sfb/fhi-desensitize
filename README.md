## FHI desensitize
This repository contains source code for a workflow implementation to remove sensitive human DNA from metagenomic samples. The workflow is implemented in Nextflow, which utilizes Anaconda to deploy/reference the software stack.  
Nextflow workflows + Anaconda environments is packaged in a container available either as Docker container, or a Singularity container via conversion

## Build:
Desensitize-wf needs the following dependencies to run:
- Docker
- (and Singularity if this is preferred)

#### Installation / build:  
Clone this repository:  
```bash
git clone  https://gitlab.com/uit-sfb/fhi-desensitize.git
```

Navigate to the cloned repository:  
```bash
cd fhi-desensitize
``` 

Build docker container  
(Total size is roughly 10GB):  
```bash
docker build -t desensitize-standalone .
```

If needed, convert the Docker container to a Singularity container:
```bash
./convert_to_singularity.sh
```
The converted conatiner will be present in the singularity/ folder

## Usage:  
##### Input filenames criteria:  
- No spaces, no commas and no dots (except .fastq.gz) in inputfilenames  
- Compressed files (fastq.gz file extension) is required

#### Docker container
A Bash script example to execute the docker container is provided in [run.sh](run.sh)

#### Singularity container
Example usage: (Replace $image and $R1 $R2)  

```bash
singularity exec \
--bind $PWD/input:/workflow/input \
--bind $PWD/output:/workflow/output \
--pwd /workflow \
--writable-tmpfs \
<image.img> \
nextflow run \
-w /workflow/output \
/workflow/desensitize.nf \
--r1 /workflow/input/$r1 \
--r2 /workflow/input/$r2  
```

#### TSD Implementation
Wrappers for running desensitize in TSD on Colossus (Slurm) for massive parallelization can be found in TSD/  
Place [job-script.sh](TSD/job-script.sh), [desensitize.sh](TSD/desensitize.sh) and your desensitize singularity **<image.img>** in the same directory somewhere on the submit node, along with and input directory containg paired end fasta files.  

```bash
Wrapper for desensitization submissions to Slurm using a Singularity container

Usage: ./desensitize.sh [parameters]
   -i|--inputdir <inputdir>	Input directory with samples to desensitize (Required)
   -m|--max <int>		Maximum simultaneous tasks at once (Default: 10)
   -c|--clean			Remove any files currently present in slurm-stdout/ and slurm-stderr/
   -d|--debug			Write MultiQC related files to desensitized-reports/
   -h|--help			Print this help text
``` 
 
(NB! The current sorting alorithm for R1/R2 samples inside <inputdir> is quite naive. Make sure every sample has a two files, and that they have "R1" and "R2" somewhere in their filename. Inspect line 44 in [desensitize.sh](https://gitlab.com/uit-sfb/fhi-desensitize/-/blob/master/TSD/desensitize.sh#L44) to change this)

## Nextflow / Anaconda specifics

#### Anaconda: Sparse software/ref overview*
On Docker build, Anaconda automatically creates an environment with the following key components inside the conatiner. This environment is referenced by Nextflow to execute workflows/tools 
| Tool/pipeline |  Version  | Note |
|----------|:-------------:|------:|
|Nextflow|20.10.40|-|
| BB-map | 38.86 | reformat.sh/repair.sh |
| Fastqc | 0.11.9 | - |
| Fastq_screen | 0.14.0 | Ref: hg38 (GRCh38)** |
| Multiqc | 1.9 | - |

*Key components, excluding other dependencies. The complete list can be found in dependencies.yaml    
**hg38 reference data is copied from https://galaxy-ntnu.bioinfo.no/reference_data/

#### Nextflow workflow overview (flattened)
The general workflow overview looks like this 
```mermaid
graph LR; reformat-->PreFastQC; PreFastQC-->Fastq_screen; Fastq_screen-->repair; repair-->PostFastQC; PostFastQC-->MultiQC; 
```
- reformat - Provides file type checks
- PreFastQC - Generates a quality report **before** desensitization
- Fastq_screen - Screens sequenced for hg38 sequence data and removes it
- repair - Resyncs fastq files
- PostFastQC - Generates a quality report **after** desensitization
- MultiQC - Wraps up all quality reports

(Some of the steps are not dependent on previous ones and will run in parallel, hence this is a flattened overview).

#### Nextflow usage
The main workflow has two parameters, --r1 and --r2.  
To run in **single-end mode**, supply ONLY the --r1 paramter like so:  
```bash
nextflow desensitize.nf --r1 <myfile.fastq.gz>
```  
For multiple single-end samples, separate with a comma, like so:  
```bash
nextflow desensitize.nf --r1 <myfile1.fastq.gz>,<myfile2.fastq.gz>,<myfile3.fastq.gz>,<myfile4.fastq.gz>
```  

To run in **paired-end mode**, supply BOTH the --r1 and --r2 parameters, like so:  
```bash
nextflow desensitize.nf --r1 <r1.fastq.gz> --r2 <r2.fastq.gz>
```  

For multiple paired-end samples, separate with a comma, like so:  
```bash
nextflow desensitize.nf --r1 <a_r1.fastq.gz>,<b_r1.fastq.gz> --r2 <a_r2.fastq.gz>,<b_r2.fastq.gz>
```  
(Make sure files are ordered, so that the first file in --r1 corresponds to the first file in --r2 and so forth)  

Add the optional parameter --cpu to set cpu usage (default: 4)  

Results will be present in the output/ folder, and includes hg38-screened and synced fastq files as well as a QC-report from multiqc

Contact:
Espen Mikal Robertsen - espen.m.robertsen@uit.no
