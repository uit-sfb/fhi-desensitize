#!/bin/bash
#SBATCH --job-name=Desensitize-p1516 
#SBATCH --account=p1516_tsd
#SBATCH --mem-per-cpu=8000M
#SBATCH --time=01:00:00
#SBATCH --export=ALL
#SBATCH --output=./slurm-stdout/slurm-%j.out
#SBATCH --error=./slurm-stderr/slurm-%j.out

## Variables needed. debug=true will also output a directory ith multiqc reports (desensitized-reports/)
R1=(`ls -1 $SLURM_SUBMIT_DIR/$dir | sort | grep R1`)
R2=(`ls -1 $SLURM_SUBMIT_DIR/$dir | sort | grep R2`)
file1=${R1[$SLURM_ARRAY_TASK_ID]}
file2=${R2[$SLURM_ARRAY_TASK_ID]}
image=desensitize-standalone_latest-2021-04-21-efe114626679.img

## Clean modules and load singularity
module purge
module load singularity/3.7.3

## Exit on errors
set -o errexit

## Prepare mount points and copy files to scratch.
cd $SCRATCH
mkdir -p $SLURM_ARRAY_TASK_ID/{input,output}
cp $SLURM_SUBMIT_DIR/$dir/$file1 $SCRATCH/$SLURM_ARRAY_TASK_ID/input
cp $SLURM_SUBMIT_DIR/$dir/$file2 $SCRATCH/$SLURM_ARRAY_TASK_ID/input

## Mark outfiles for automatic copying to $SLURM_SUBMIT_DIR. If debug=true, also mark reports directory
chkfile desensitized
if [ "$debug" = true ]; then
    chkfile desensitized-reports
fi

## Run command for songularity container
singularity exec \
--bind $SCRATCH/$SLURM_ARRAY_TASK_ID/input:/workflow/input \
--bind $SCRATCH/$SLURM_ARRAY_TASK_ID/output:/workflow/output \
--pwd /workflow \
--writable-tmpfs \
$SLURM_SUBMIT_DIR/$image \
nextflow run \
-w /workflow/output \
/workflow/desensitize.nf \
--r1 /workflow/input/$file1 \
--r2 /workflow/input/$file2

## Prepare output for rsync as referenced by "chkfile"
mkdir -p desensitized
mv $SLURM_ARRAY_TASK_ID/output/fastq/* desensitized

## If debug=true, also prepare multiqc reports for output
if [ "$debug" = true ]; then
    tmp1=$(basename $file1 .fastq.gz)
    tmp2=$(basename $file2 .fastq.gz)
    concat="$tmp1$tmp2"
    mkdir -p desensitized-reports/$concat
    mv $SLURM_ARRAY_TASK_ID/output/multiqc/multiqc/multiqc_data/multiqc_fastqc.txt desensitized-reports/$concat
    mv $SLURM_ARRAY_TASK_ID/output/multiqc/multiqc/multiqc_data/multiqc_fastq_screen.txt desensitized-reports/$concat
    mv $SLURM_ARRAY_TASK_ID/output/multiqc/multiqc/multiqc_data/multiqc_general_stats.txt desensitized-reports/$concat
    mv $SLURM_ARRAY_TASK_ID/output/multiqc/multiqc/multiqc_report.html desensitized-reports/$concat
fi
