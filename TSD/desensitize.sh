#!/bin/bash

Help()
{
echo ""
echo "Wrapper for desensitization submissions to Slurm using a Singularity container"
echo ""
echo "Usage: ./desensitize.sh [parameters]"
echo "   -i|--inputdir <inputdir>	Input directory with samples to desensitize (Required)"
echo "   -m|--max <int>		Maximum simultaneous tasks at once (Default: 10)"
echo "   -c|--clean			Remove any files currently present in slurm-stdout/ and slurm-stderr/"
echo "   -d|--debug			Write MultiQC related files to desensitized-reports/"
echo "   -h|--help			Print this help text"
echo ""
}

# Default variables
max=10
debug=false
clean=false

# Check if no parameters. If so, print Help
if [ $# -eq 0 ]; then
    Help; exit 1
fi

# Parse parameters
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -i|--inputdir) inputdir="$2"; shift ;;
        -m|--max) max="$2"; shift ;;
        -d|--debug) debug=true ;;
        -c|--clean) clean=true ;;
        -h|--help) Help; exit;;
        *) echo "Unknown parameter passed: $1"; Help; exit 1 ;;
    esac
    shift
done

# Check whether -i|--inputdir is parsed, and that the directory actually exists in $PWD
[ -z $inputdir ] && echo "-i|--inputdir is required." && exit 1
[ -d $inputdir ] || { echo "$inputdir does not exist!" && exit 1; }

# Make two arrays containing R1 and R2 files from $1. Change this expression as appropriate
R1=(`ls -1 $inputdir | sort | grep R1`)
R2=(`ls -1 $inputdir | sort | grep R2`)

# Clean
if [ "$clean" = true ]; then
    rm -f slurm-stdout/*
    rm -f slurm-stderr/*
fi


# Variables needed by job-script.sh
export dir=$inputdir
if [ "$debug" = true ]; then
    export debug=true
fi

### Checks
# Check that arrays are of equal length (every R1 has an R2), else exit
if [ "${#R1[@]}" -ne "${#R2[@]}" ]; then
echo "Every R1 does not have an R2, or vice versa. Exiting."
exit
fi

# TODO: Check that each file has a partner based on filename

### Set bounds for sbatch --array
bottom=0
top=${#R1[@]}
top=$((top-1))

sbatch --array=$bottom-$top%$max job-script.sh
