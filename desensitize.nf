#!/usr/bin/env nextflow
// DSL 2 support and thread configuration. Default is 4 threads, provide --cpu on command line to change it. r2 defaults to null to supress warnings
nextflow.enable.dsl=2
params.cpu=4
params.r2=null

// Include modules for each tool
include {Paired_end} from './pipelines/paired_end.nf'
include {Single_end} from './pipelines/single_end.nf'

// - Criteria:
// - No spaces - No commas - No dots in filenames (other than in file endings .fastq.gz)
// - Multiple samples are provided with comma separation in a sorted fashion. (ex: --r1 r1.1,r1.2,r1.3 --r2 r2.1,r2.2,r2.3)
// - Paired-end and single-end samples (workflow executions) are mutually exclusive

workflow{

  // Paired end - params.r1 and params.r2 needs to be defined and have the same length after split on ',' (for multiple samples)
  if (params.r1 && params.r2){
    println("Paired-end - Initializing...")
    r1 = params.r1.tokenize(',')
    r2 = params.r2.tokenize(',')
    if (r1.size() == r2.size()){
      println("Paired-end - All files have pairs")
      println("Paired-end - Processing $r1.size samples, ${r1.size*2} files in total")
      r1 = Channel.fromPath(r1).flatMap{files(it)}
      r2 = Channel.fromPath(r2).flatMap{files(it)}
      
      Paired_end(r1,r2)
    }
    else{
      println("Paired-end - Some files are relicts! Check input syntax.");
    }
  }
  
  // Single end - when only params.r1 is defined
  else if (params.r1){
    println("Single-end - Initializing...")
    r1 = params.r1.tokenize(',')
    println(r1)
    println("Single-end - Processing $r1.size samples, $r1.size files in total")
    r1 = Channel.fromPath(r1).flatMap{files(it)}
    Single_end(r1)
  }
  
  // If parameters are malformed - Complain
  else {
    println("Wrong parameters - Please see url for documentation on how to use this service");
  }
}
