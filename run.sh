docker run \
	-v $(pwd)/output:/workflow/output \
	desensitize-standalone \
	nextflow desensitize.nf \
	--r1 output/a_r1.fastq.gz \
	--r2 output/a_r2.fastq.gz
